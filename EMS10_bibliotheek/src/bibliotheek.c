/*
 * bibliotheek.c
 *
 *  Created on: 14 feb. 2018
 *     Authors: VersD / BroJZ
 */

#include <msp430.h>
#include "../inc/bibliotheek.h"

bool zet_klok_op_MHz(uint8_t mhz)
{
    DCOCTL = 0;

    if (CALBC1_1MHZ==0xFF || CALBC1_8MHZ==0xFF || CALBC1_12MHZ==0xFF || CALBC1_16MHZ==0xFF)
    {
        return false;
    }

    switch (mhz)
    {
    case 1:
        BCSCTL1 = CALBC1_1MHZ; // Set range
        DCOCTL = CALDCO_1MHZ;  // Set DCO step + modulation */
        break;
    case 8:
        BCSCTL1 = CALBC1_8MHZ; // Set range
        DCOCTL = CALDCO_8MHZ;  // Set DCO step + modulation */
        break;
    case 12:
        BCSCTL1 = CALBC1_12MHZ; // Set range
        DCOCTL = CALDCO_12MHZ;  // Set DCO step + modulation */
        break;
    case 16:
        BCSCTL1 = CALBC1_16MHZ; // Set range
        DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */
        break;
    default:
        return false;
    }

    return true;
}

void zet_pin_richting(uint8_t poort, uint8_t pin, Richting richting)
{
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if (richting == input)
            {
                P1DIR &= ~(1<<pin);
            }
            else if (richting == output)
            {
                P1DIR |= 1<<pin;
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if (richting == input)
            {
                P2DIR &= ~(1<<pin);
            }
            else if (richting == output)
            {
                P2DIR |= 1<<pin;
            }
        }
    }
}

void output_pin(uint8_t poort, uint8_t pin, Waarde waarde)
{
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if (waarde == laag)
            {
                P1OUT &= ~(1<<pin);
            }
            else if (waarde == hoog)
            {
                P1OUT |= 1<<pin;
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if (waarde == laag)
            {
                P2OUT &= ~(1<<pin);
            }
            else if (waarde == hoog)
            {
                P2OUT |= 1<<pin;
            }
        }
    }
}

void zet_interne_weerstand(uint8_t poort, uint8_t pin, Weerstand weerstand)
{
    /*
     * Deze functie zet een interne weerstand bij een GPIO input pin aan of uit
     * Parameters:
     * - poort: het betreffende poortnummer (1 of 2)
     * - pin: het betreffende pinnummer (0 t/m 7 voor poort 1 of 0 t/m 5 voor poort 2)
     * - weerstand: het gewenste type (none, pull_up of pull_down)
     */
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if (weerstand == none)
            {
                P1DIR &= ~(1<<pin);
                P1REN &= ~(1<<pin);
            }
            else if (weerstand == pull_up)
            {
                P1DIR &= ~(1<<pin);
                P1REN |= 1<<pin;
                P1OUT |= 1<<pin;
            }
            else if (weerstand == pull_down)
            {
                P1DIR &= ~(1<<pin);
                P1REN |= 1<<pin;
                P1OUT &= ~(1<<pin);
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if (weerstand == none)
            {
                P2DIR &= ~(1<<pin);
                P2REN &= ~(1<<pin);
            }
            else if (weerstand == pull_up)
            {
                P2DIR &= ~(1<<pin);
                P2REN |= 1<<pin;
                P2OUT |= 1<<pin;
            }
            else if (weerstand == pull_down)
            {
                P2DIR &= ~(1<<pin);
                P2REN |= 1<<pin;
                P2OUT &= ~(1<<pin);
            }
        }
    }
}

int input_pin(uint8_t poort, uint8_t pin)
{
    /*
     * Deze functie leest de waarde van een GPIO input pin
     * Parameters:
     * - poort: het betreffende poortnummer (1 of 2)
     * - pin: het betreffende pinnummer (0 t/m 7 voor poort 1 of 0 t/m 5 voor poort 2)
     * Returnwaarde: true als de input pin hoog is of false als de input pin laag is
     */
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if ((P1IN & 1<<pin) != 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if ((P2IN & 1<<pin) != 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}

int debounce_knop(uint8_t poort, uint8_t pin)
{
    uint16_t integrator;
    uint8_t gefilterdeKnop;
    const uint16_t MAXIMUM = 20;

    if (poort == 1)
    {
        if (pin <= 7)
        {
            while(1)
            {
                if(P1IN & 1<<pin)
                {
                    if(integrator < MAXIMUM)
                    {
                        integrator++;
                    }
                }
                else
                {
                    if(integrator > 0)
                    {
                        integrator--;
                    }
                }
                if(integrator == MAXIMUM)
                {
                    gefilterdeKnop = 1;
                    return gefilterdeKnop;
                }
                else if(integrator == 0)
                {
                    gefilterdeKnop = 0;
                    return gefilterdeKnop;
                }
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            while(1)
            {
                if(P1IN & 1<<pin)
                {
                    if(integrator < MAXIMUM)
                    {
                        integrator++;
                    }
                }
                else
                {
                    if(integrator > 0)
                    {
                        integrator--;
                    }
                }
                if(integrator == MAXIMUM)
                {
                    gefilterdeKnop = 1;
                    return gefilterdeKnop;
                }
                else if(integrator == 0)
                {
                    gefilterdeKnop = 0;
                    return gefilterdeKnop;
                }
            }
        }
    }
}
