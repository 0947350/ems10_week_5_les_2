#include <msp430.h> 
#include "inc/bibliotheek.h"

/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer
	
	// initialiseer de klok op 1 MHz
	// bekijk of false wordt teruggegeven
	if (zet_klok_op_MHz(1) == false)
	{
	    while (1); // blijf hier hangen zodat dit wordt opgemerkt met het debuggen
	}

	// SW0
	zet_pin_richting(1, 0, input);
	zet_interne_weerstand(1, 0, pull_down);
	// SW1
	zet_pin_richting(1, 1, input);
	zet_interne_weerstand(1, 1, pull_down);

	// Rood
	zet_pin_richting(2, 0, output);
	// Groen
	zet_pin_richting(2, 1, output);
	// Blauw
	zet_pin_richting(2, 2, output);

    int SW0_staat = 0;
    int oude_SW0_staat = 0;
    int SW0_geraakt = 0;

    int SW1_staat = 0;
    int oude_SW1_staat = 0;
    int SW1_geraakt = 0;

    typedef enum {op_slot, dicht, open} toestand;

    toestand huidigeToestand = op_slot;

	while (1)
	{

        SW0_staat = debounce_knop(1, 0);
        SW1_staat = debounce_knop(1, 1);

        if (SW0_staat == 1 && oude_SW0_staat == 0)
        {
            if (SW0_geraakt == 0 && huidigeToestand == op_slot)
            {
                SW0_geraakt = 1;
            }
            else if (SW0_geraakt == 1 && huidigeToestand == dicht)
            {
                SW0_geraakt = 0;
            }
        }
        if (SW1_staat == 1 && oude_SW1_staat == 0)
        {
            if (SW1_geraakt == 0 && huidigeToestand == dicht)
            {
                SW1_geraakt = 1;
            }
            else if (SW1_geraakt == 1 && huidigeToestand == open)
            {
                SW1_geraakt = 0;
            }
        }

	    switch(huidigeToestand)
	    {
            case op_slot:
                output_pin(2, 0, hoog), output_pin(2, 1, laag), output_pin(2, 2, laag);
                if(SW0_geraakt == 1) huidigeToestand = dicht;
                break;
            case dicht:
                output_pin(2, 0, laag), output_pin(2, 1, laag), output_pin(2, 2, hoog);
                if(SW0_geraakt == 0) huidigeToestand = op_slot;
                if(SW1_geraakt == 1) huidigeToestand = open;
                break;
            case open:
                output_pin(2, 0, laag), output_pin(2, 1, hoog), output_pin(2, 2, laag);
                if(SW1_geraakt == 0) huidigeToestand = dicht;
                break;
	    }

	    oude_SW0_staat = SW0_staat;
	    oude_SW1_staat = SW1_staat;
	}
}
